const express = require('express');
const { viewMedios, newMedioDePago, deletMedio, updateMedio } = require('../data/medios');
const { authAdm } = require('../middleware/authAdm');


function getRouterMedios() {
  const router = express.Router();
  router.get('/', authAdm, viewMedios);
  router.post('/', authAdm, newMedioDePago);
  router.delete('/:id', authAdm, deletMedio);
  router.put('/:id', authAdm, updateMedio);

  return router;
}





module.exports = {
  getRouterMedios
}