
const express = require('express');
const { viewProduct, newProduct, deletProduct, updateProduct } = require('../data/productos');
const { authAdm } = require('../middleware/authAdm');


function getRouterProduct() {
  const router = express.Router();
  router.get('/', viewProduct);
  router.post('/', authAdm, newProduct);
  router.delete('/:id', authAdm, deletProduct);
  router.put('/:id', authAdm, updateProduct);

  return router;
}




module.exports = {
  getRouterProduct
}