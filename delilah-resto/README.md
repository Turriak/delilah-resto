# API Rest Delilah Restó

Esta es una API básica para gestiona pedidos del restaurante Delilah Restó correspondiente al Sprint 1.

# Instalación 

Esta API corre sobre NodeJS

[NodeJS](https://nodejs.org/en/ 'Sitio oficial')

A continucación se detalla las librerias necesarias para la implementación de la API Delilah Restó.
* [Express](http://expressjs.com/) 
* [Swagger](https://swagger.io/)
* [YAML](https://yaml.org/)    

_Instalamos las siguientes libreria del repositorio npm con los siguientes comandos_

```
npm install express
```

```
npm install swagger-ui-express
```
```
npm i js-yaml
```
_Inicie el servido con el comando_

```
npm start
```
_Por defecto el servidor iniciado escuchara el puerto **8080**, este parametro puede ser modificado desde la constante **port** dentro del archivo **main.js**_


# Despliegue 

## Testing
Para la realización del testing se integro la documentación con swagger, a continuación se deja enlace correspondiente.  El mismo estara disponible una vez que corra el servidor.

* [API - Documentación](http://localhost:8080/api-doc/)


## Usuario por defecto

Para realizar testing se crearon por defecto un usuario con privilegios de administrador y un usuario cliente del restó.
### Usuario Administrador
* **User name:** turria
* **Password:** 123
### Usuario Cliente
* **User name:** acamica
* **Password:** 321

### Sistema de logeo

Para el sistema de login se implemento el uso de un token, el mismo es devuelto al momento de hacer login junto con el ID de usuario, este token y el id será requerido por los demás endpoint como parametro de cabezera (headers).

## Construido con las tecnologías de:

* NodeJS
* Libreria Express (repositorio npm)
* Documentador Swagger

## Autor 

* _**Daniel A. Santos**_

## Licencia

* MIT




