
const express = require('express');
const { loginUser, newUser } = require('../data/usuarios');


function getRouterLogin() {
  const router = express.Router();
  router.post('/login', loginUser);
  router.post('/register', newUser);
  return router;
}




module.exports = {
  getRouterLogin
}

