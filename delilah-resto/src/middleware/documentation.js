const yaml = require('js-yaml');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');

// --== Carga el yaml al servidor ==--
function loadSwaggerInfo(server) {
  try {
    const doc = yaml.load(fs.readFileSync('./src/spec.yml', 'utf-8'));
    server.use('/api-doc', swaggerUi.serve, swaggerUi.setup(doc));
  } catch (e) {
    console.log(e)
  }
}
// --== ------------------------- ==--


module.exports = {
  loadSwaggerInfo
}