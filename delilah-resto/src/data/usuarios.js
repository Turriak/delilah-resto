let users = require('../user.json');
let login = new Array();

// --== Crear nuevo usuario ==--
function newUser(req, res) {
  const datos = req.body;
  if (datos.usuario && datos.nomyape && datos.email && datos.telefono && datos.direccion && datos.password) {
    for (const user of users) {
      if (user.usuario === datos.usuario || user.email === datos.email) {
        res.send("Usuario o email ya registrados");
        return;
      }
    }
    users.push(datos);
    res.status(200).send("Usuario registrado exitosamente");
  } else {
    res.status(401).send("Carga de datos invalida");
  }
}
// --== ------------------- ==--


// --== Logea un usuario por mail o username ==--
function loginUser(req, res) {
  const username = req.body.username;
  const pass = req.body.pass;
  let i = 0;
  for (i in users) {
    if (((users[i].usuario === username) || (users[i].email === username)) && (users[i].password === pass)) {
      for (const l of login) {
        if (l.id === Number(i)) {
          res.status(200).send(`Usuario ya logeado toke: ${l.token} ID: ${l.id}`);
          return;
        }
      }
      const token = new Date().getTime();
      login.push({ "token": token, "id": Number(i) });
      res.status(200).send(`Usuario logeado exitosamente token: ${token} ID: ${i}`);
      return;
    }
  }
  res.status(404).send("Usuario no registrado");
}
// --== ------------------------------------ ==--


// --== Lista los usuarios registrados ==--
function viewUsers(req, res) {
  res.status(200).send(users);
}
// --== ------------------------------ ==--


module.exports = {
  newUser,
  loginUser,
  viewUsers,
  users,
  login
}


