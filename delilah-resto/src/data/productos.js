let productos = require('../product.json')


// --== Lista los usuarios registrados ==--
function viewProduct(req, res) {
    res.send(productos);
}
// --== ------------------------------ ==--


// --== Carga un nuevo producto ==--
function newProduct(req, res) {
    if (productos.length === 0 ) {
        newIdPro = 1;
    } else {
        newIdPro = productos[productos.length -1].Idproducto +1;
    }
    const newDes = req.body.descripcion;
    const newPre = Number(req.body.precio);
    productos.push({"Idproducto":newIdPro, "descripcion": newDes,"precio":newPre});
    res.status(200).send("Producto cargado exitosamente");
    return;
}
// --== ---------------------- ==--


// --== Eliminar productos x ID producto ==--
function deletProduct(req, res) {
    const idPro = Number(req.params.id);
    for (const i of productos) {
        if (i.Idproducto == idPro) {
            const pos = productos.indexOf(i);
            productos.splice(pos, 1);
            res.status(200).send('Producto ha sido eleminado ');
            return;
        }
    }
    res.status(404).send('Producto no encontrado');
}
// --== -------------------------------- ==--


// --== Modificar productos x ID  ==--
function updateProduct(req, res) {
    const idPro = Number(req.params.id);
    for (const i of productos) {
        if (i.Idproducto === idPro) {
           i.descripcion = req.body.descripcion;
           i.precio = Number(req.body.precio);
           res.status(200).send(`Producto ha sido eleminado + ${i}`);
           return;
        }
    }
    res.status(404).send('Producto no encontrado');
}
// --== -------------------------- ==--




module.exports = {
    productos,
    viewProduct,
    newProduct,
    updateProduct,
    deletProduct
}