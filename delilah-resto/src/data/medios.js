let mediosDePago = require('../medios.json');


// --== Carga un medio de Pago ==--
function newMedioDePago(req, res) {
    if (mediosDePago.length === 0 ) {
        newIdMed = 1;
    } else {
        newIdMed = mediosDePago[mediosDePago.length - 1].idMedio + 1;
    }
    const newDet = req.body.detalle;
    mediosDePago.push({ "idMedio": newIdMed, "detalle": newDet });
    res.status(200).send("Medio de pago cargado exitosamente");
    return;
}
// --== ---------------------- ==--


// --== Lista medios de Pago ==--
function viewMedios(req, res) {
    res.status(200).send(mediosDePago);
}
// --== -------------------- ==--


// --== Eliminar medio de Pago x ID ==--
function deletMedio(req, res) {
    const idMed = Number(req.params.id);
    for (const i of mediosDePago) {
        if (!!i.idMedio && i.idMedio === idMed ) {
            const pos = mediosDePago.indexOf(i);
            mediosDePago.splice(pos, 1);
            res.send('El medio de pago ha sido eliminado');
            return;
        }
    }
    res.status(404).send('Medio de pago no encontrado');
}
// --== -------------------------------- ==--


// --== Modificar Medios de Pago x ID  ==--
function updateMedio(req, res) {
    const idMed = Number(req.params.id);
    for (const i of mediosDePago) {
        if (!!i.idMedio && i.idMedio === idMed) {
           i.detalle = req.body.detalle;
           res.status(200).send('Medio de pago modificado exitosamente');
           return;
        }
    }
    res.status(404).send('Medio de pago no encontrado');
}
// --== -------------------------- ==--



module.exports = {
    newMedioDePago,
    viewMedios,
    deletMedio,
    updateMedio
}