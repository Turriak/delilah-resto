const { users } = require('../data/usuarios');
const { productos } = require('../data/productos');

let pedidos = require('../pedidos.json');


// --== Listar array de pedidos ==-
function viewPedidos(req, res) {
  res.send(pedidos);
}
// --== ----------------------- ==-


// --== Ver Historial de Pedidos ==--
function viewHistory(req, res) {
  const idUser = Number(req.headers.iduser);
  let historial = [];
  for (const i of pedidos) {
    if (i.idusuario === idUser) {
      historial.push(i);
    }
  }
  res.send(historial);
}
// --== ------------------------ ==--


// --== Modificar estado del pedido x ID ==--
function updateEstado(req, res) {
  const newEstado = Number(req.headers.estado);
  const idPed = Number(req.params.id);
  for (const i of pedidos) {
    if (idPed === i.idPedido) {
      if (i.estado > 1) {
        i.estado = newEstado;
        res.send(`Estado cambiado exitosamente`);
        return;
      } else {
        res.send(`El pedido aún no se cerro`);
        return;
      }
    }
  }
  res.status(404).send('Pedido no encontrado');
}
// --== -------------------------------- ==--


// ----================  Generar Pedido  ================----
// --== Genera el nuevo detalle del pedido ==--
function newDetalle(datos) {
  let arrayDetalle = [];
  for (const i of datos) {
    arrayDetalle.push(
      {
        "cantidad": i.cantidad,
        "idProducto": i.idProducto,
        "precio": precio(i.idProducto) * i.cantidad
      });
  }
  return arrayDetalle;
}
// --== --------------------------------------------- ==-


// --== Busca el precio del producto del array Productos ==--
function precio(idproduct) {
  for (const i of productos) {
    if (idproduct === i.Idproducto) {
      return i.precio;
    }
  }
}
// --== ----------------------------------------------- ==-


// --== Suma total del pedido ==--
function precioTotal(dato) {
  let total = 0;
  for (const i of dato) {
    total = total + i.precio;
  }
  return total;
}
// --== --------------------------------------------- ==-


// --== Genera el nuevo pedido ==--
function newPedido(req, res) {
  let newDir;
  let newIdped;
  const idUser = Number(req.headers.iduser);
  const newEstado = Number(req.body.estado);
  const newMedioPag = Number(req.body.medioDePago);
  const newHoraPedido = new Date();
  const newDet = newDetalle(req.body.detalle);
  if (pedidos.length === 0) {
    newIdped = 1;
  } else {
    newIdped = Number(pedidos[pedidos.length - 1].idPedido + 1);
  }
  if (req.body.dirOpEstado) {
    newDir = req.body.dirOp;
  } else {
    newDir = users[idUser].direccion;
  }
  pedidos.push({
    "idPedido": Number(newIdped),
    "idusuario": Number(idUser),
    "horaPedido": newHoraPedido,
    "usuario": users[idUser].usuario,
    "estado": Number(newEstado),
    "medioDePago": Number(newMedioPag),
    "total": Number(precioTotal(newDet)),
    "direccion": newDir,
    "detalle": newDet
  });
  res.send("Pedido cargado");
  return;
}
// --== --------------------------------------------- ==--
// ----================ End of Generar Pedido  ================----

// --== Modificar un pedidod aun no cerrado ==--
function updatePedido(req, res) {
  const numPedido = Number(req.headers.pedido);
  const newEstado = Number(req.body.estado);
  const newDet = newDetalle(req.body.detalle);
  for (const i of pedidos) {
    if (numPedido === i.idPedido) {
      i.estado = Number(newEstado);
      i.detalle = newDet;
      i.total = Number(precioTotal(newDet));
      res.send("Pedido Modificacdo exitosamente");
      return;
    }
  }
}




module.exports = {
  viewPedidos,
  viewHistory,
  updateEstado,
  newPedido,
  updatePedido,
  pedidos
}