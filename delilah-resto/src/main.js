const express = require('express');
const { authAdm } = require('./middleware/authAdm');
const { authLogin } = require('./middleware/authLogin');
const { loadSwaggerInfo } = require('./middleware/documentation');
const { getRouterLogin } = require('./routers/login');
const { getRouterMedios } = require('./routers/medios');
const { getRouterPedidos } = require('./routers/pedidos');
const { getRouterProduct } = require('./routers/product');
const { getRouterUser } = require('./routers/user');
const port = 8080;




function main() {
  const server = express();
  server.use(express.json());
  loadSwaggerInfo(server);

  server.use('/api/v1', getRouterLogin());
  server.use('/api/v1/usuarios', authLogin, getRouterUser());
  server.use('/api/v1/productos', authLogin, getRouterProduct());
  server.use('/api/v1/medios', authLogin, getRouterMedios());
  server.use('/api/v1/pedidos', authLogin, getRouterPedidos());


  server.listen(port, () => {
    console.log(`Server ready in Port: ${port}`);
  });
}

main();