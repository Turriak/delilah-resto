const {login} = require('../data/usuarios');


// --== Autentifija que este logeado el usuario ==--
function authLogin(req, res, next) {
    const idUser = Number(req.headers.iduser);
    const token = Number(req.headers.token);
    
    for (const i of login) {
        if (idUser === i.id && token === i.token) {
            next();
            return;
        }
    }
    res.status(401).send('Debe logearse primero');
}
// --== --------------------------------------- ==--



module.exports = {
    authLogin
}
