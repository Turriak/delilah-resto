
const express = require('express');
const { viewPedidos, viewHistory, updateEstado, newPedido, updatePedido } = require('../data/pedidos');
const { authAdm } = require('../middleware/authAdm');
const { pedidoPendiente, pedidoCerrado } = require('../middleware/estadoPedido');


function getRouterPedidos() {
  const router = express.Router();
  router.get('/', authAdm, viewPedidos);
  router.get('/historial', viewHistory);
  router.put('/:id', authAdm, pedidoCerrado, updateEstado);
  router.post('/', pedidoCerrado, newPedido);
  router.put('/', pedidoPendiente, updatePedido);
  //router.delete('/:id', authAdm, );

  return router;
}




module.exports = {
  getRouterPedidos
}