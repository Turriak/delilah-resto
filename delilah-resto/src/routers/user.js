
const express = require('express');
const { viewUsers } = require('../data/usuarios');
const { authAdm } = require('../middleware/authAdm');


function getRouterUser() {
  const router = express.Router();
  router.get('/',authAdm, viewUsers );
  return router;
}




module.exports = {
    getRouterUser
  }