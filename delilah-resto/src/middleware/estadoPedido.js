const { pedidos } = require('../data/pedidos');

function pedidoCerrado(req, res, next) {
  const idUser = Number(req.headers.iduser);
  for (let i of pedidos) {
    if (idUser === i.idusuario && i.estado === 1) {
      res.send('Debe cerrar el pedido pendiente antes de generar uno nuevo');
      return;
    }
  }
  next();
}

function pedidoPendiente(req, res, next) {
  const idUser = Number(req.headers.iduser);
  for (const i of pedidos) {
    if (idUser === i.idusuario && i.estado === 1) {
      req.headers.pedido = i.idPedido;
      next();
      return;
    }
  }
  res.send('No tiene pedidos pendiende de modificar');
}



module.exports = {
  pedidoCerrado,
  pedidoPendiente
}