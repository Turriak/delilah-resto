const {users} = require('../data/usuarios');


// --== Autentifica que seas Adm ==--
function authAdm(req, res, next) {
    const idUser = req.headers.iduser;
    if (users[idUser].admin) {
        next();
        return;
    }
    res.send('No tiene privilegios');
}
// --== --------------------------------------- ==--


module.exports = {
    authAdm
}
